<!DOCTYPE html>
<html>
  <head>
    <title>Now Showing</title>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="skeleton.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Montserrat|Monoton" rel="stylesheet">
    <script src="https://use.fontawesome.com/f78d37804b.js"></script>
    <script type="text/javascript"></script>
  </head>


<body ondblclick="window.scrollTo(0,0);">

<header>
  <div class="header"><img id="silverado" a href="https://ibb.co/gHzkpv"><img src="https://image.ibb.co/meKUGa/logofinal.png" alt="Silverado Cinema" border="0"/> Silverado Cinema
  </div>
  <!-- Logo + business name -->
</header>

<nav>
  <div class="navbar">
    <ul>
        <li><a href="#home"<i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
        <li><a href="#showing" <i class="fa fa-film" aria-hidden="true"></i> Now Showing</a></li>
        <li><a href="#tickets"<i class="fa fa-ticket" aria-hidden="true"></i> Tickets</a></li>
      </ul>
  </div>
</nav>


<main>

<!-- Carousel -->
<div class="u-full-width container">
  <div id="1" class="one-half column hideable" style="display: block;"><a href="https://ibb.co/h1t0NF"><img src="https://image.ibb.co/m8opGa/pexels_photo_236171tradition.jpg" alt="Tradition" border="0">
  <h2>Tradition</h2> <br> <h3>Oscar for Best Foreign Language Film</h3>
  </div>

  <div id="2" class="one-half column hideable "><a href="https://ibb.co/fC5EhF"><img src="https://image.ibb.co/czB9Ga/pexels_photo_46024cat.jpg" alt="Cats and Dogs" border="0"></a>
  <h2>Cats and Dogs</h2> <br> <h3>An unexpected friendship in the search for a home</h3>
  </div>

  <div id="3" class="one-half column hideable"><a href="https://ibb.co/eSL3ba"><img src="https://image.ibb.co/n3arUv/pexels_photo_70737love.jpg" alt="pexels_photo_70737love" border="0"></a>
  <h2>A Perfect Afternoon</h2> <br> <h3>Romance is in the air</h3>
  </div>
  
  <div id="4" class="one-half column hideable"><a href="https://ibb.co/gA25pv"><img src="https://image.ibb.co/mopkpv/war_desert_guns_gunshow_163347.jpg" alt="war_desert_guns_gunshow_163347" border="0"></a>
  <h2>Doomsday Clock</h2> <br> <h3>The end is near</h3>
  </div>
</div>

<!-- Back/Foward Carousel -->
<form>
<input type="button" value="Prev">/>
<input type="button" value="Next" />
</form>


<div class="container">


    <h1>&star; Web Programming Form Tester &star;</h1>
    <hr><h2>HTTP_REFERER Notice</h2><p>This processing script provides detailed advice if you submit data from a web form.</p><p>Make sure that you set your form's action attribute to "<span class='r'>/~e54061/wp/silverado-test.php</span>" for detailed feedback.</p>    <hr/>

    <h2>&lt;form method='post' ... &gt;</h2>
    <p><span>Nothing has been submitted using the post method.</span></p>    <h2>&lt;/form&gt;</h2>
    <hr/>

    <h2>&lt;form method='get' ... &gt;</h2>
    <p><span>Nothing has been submitted using the get method.</span>    <h2>&lt;/form&gt;</h2>
    <hr style="margin-bottom:50px;"/>

  </div>
  <!-- Here's what our last "developer" left us, hope it helps! <i>- Silverado crew -->
  <!-- Starting form code sourced and adapted from https://titan.csit.rmit.edu.au/~e54061/wp/silverado-test.php -->
<div class="container">

  <form method='post'>
    <fieldset><legend>Booking Form</legend>
      <p><label>Movie</label><select name="Movie" required></select></p>
          <option selected disabled>Movie</option>
          <option value="01">Tradition</option>
          <option value="02">Cats and Dogs</option>
          <option value="03">A Perfect Afternoon</option>
          <option value="04">Doomsday Clock</option>
      <p><label>Session</label><select name="Session" required></select></p>
      <fieldset><legend>Seats</legend>
        <fieldset><legend>Standard</legend>
          <p><label>Adult</label><select name=TBA></select></p>
          <p><label>Concession</label><select name=TBA></select></p>
          <p><label>Child</label><select name=TBA></select></p>
        </fieldset>
        <fieldset><legend>First Class</legend>
          <p><label>Adult</label><select name=TBA></select></p>
          <p><label>Child</label><select name=TBA></select></p>
        </fieldset>
        <fieldset><legend>Bean Bags</legend>
          <p><label>Adult</label><select name=TBA></select></p>
          <p><label>Family</label><select name=TBA></select></p>
          <p><label>Child</label><select name=TBA></select></p>
        </fieldset>
      </fieldset>
      <p><button>Book</button></p>
    </fieldset>
  </form>
</div>
  <!-- Standard buttons -->

<div class="container">
<a class="button" href="#">Anchor button</a>
<button>Button element</button>
<input type="submit" value="submit input">
<input type="button" value="button input">

<!-- Primary buttons -->
<a class="button button-primary" href="#">Anchor button</a>
<button class="button-primary">Button element</button>
<input class="button-primary" type="submit" value="submit input">
<input class="button-primary" type="button" value="button input">
SUBMIT
<!-- The above form looks like this -->
<form>
  <div class="row">
    <div class="six columns">
      <label for="exampleEmailInput">Your email</label>
      <input class="u-full-width" type="email" placeholder="test@mailbox.com" id="exampleEmailInput">
    </div>
    <div class="six columns">
      <label for="exampleRecipientInput">Reason for contacting</label>
      <select class="u-full-width" id="exampleRecipientInput">
        <option value="Option 1">Questions</option>
        <option value="Option 2">Admiration</option>
        <option value="Option 3">Can I get your number?</option>
      </select>
    </div>
  </div>
  <label for="exampleMessage">Message</label>
  <textarea class="u-full-width" placeholder="Hi Dave …" id="exampleMessage"></textarea>
  <label class="example-send-yourself-copy">
    <input type="checkbox">
    <span class="label-body">Send a copy to yourself</span>
  </label>
  <input class="button-primary" type="submit" value="Submit">
</form>

</div>
<!-- Always wrap checkbox and radio inputs in a label and use a <span class="label-body"> inside of it -->

<!-- Note: The class .u-full-width is just a utility class shorthand for width: 100% -->
  <!-- End of Starting form code -->
</main>

<footer>
    <span><h3>&copy; Mariane Dinis (s3672108) and Ahmet Cem Erdem (s3676200).<script>document.write(new Date().getFullYear());
    </script><br>Images edited with <a href="http://www198.lunapic.com/editor/">LunaPic</a><br>Cinema graphic by <a href="http://www.flaticon.com/authors/freepik">freepik</a> from <a href="http://www.flaticon.com/">Flaticon</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a>. Check out the new logo that I created on <a href="http://logomakr.com" title="Logo Maker">LogoMaker.com</a> https://logomakr.com/3CB7oF3CB7oF</h3></span>
</footer>

</body>

</html>