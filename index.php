<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <!-- meta http-equiv="X-Frame-Options" content="deny" -->
	<title>Siverado Cinema</title>
	<link rel="stylesheet" type="text/css" href="index.css">
	<link rel="stylesheet" type="text/css" href="skeleton.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto|Montserrat|Monoton" rel="stylesheet">
	<script src="https://use.fontawesome.com/f78d37804b.js"></script>
</head>

<body>

<header>
	<div class="header"><img id="silverado" a href="https://ibb.co/gHzkpv"><img src="https://image.ibb.co/meKUGa/logofinal.png" alt="Silverado Cinema" border="0"/> Silverado Cinema
	</div>
	<!-- Logo + business name -->
</header>

<nav>
	<div class="navbar">
		<ul>
			<li><a href="#home"<i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
			<li><a href="#showing" <i class="fa fa-film" aria-hidden="true"></i> Now Showing</a></li>
			<li><a href="#tickets"<i class="fa fa-ticket" aria-hidden="true"></i> Tickets</a></li>
	  	</ul>
	</div>
</nav>

<main>
<!-- .container is main centered wrapper -->
<div class="container">
  <div class="row">
    <div id="cinema" class="five columns"><a href="https://ibb.co/dp9JRa"><img src="https://image.ibb.co/geuUXF/imageedit_1_7953842159.jpg" alt="imageedit_1_7953842159" border="0" width="350"></a></div>.
    
    <div class="seven columns">
    <h1>Time For Change</h1>
    <h2>Silverado proudly presents its new 3D rooms</h2>
    <p>Forget traveling big distances just to watch a movie on 3D. Now we bring the latest technology next to you. Silverado Cinema is lauching its newest rooms with 3D, with the best image and sound quality you've ever experienced. </p></div>
  </div>
</div>

</main>

<footer>
	<div>
	Silverado Cinemas<br>
	160 - 192 Main Street, Bacchus Marsh VIC 3340
	<br>
	<a href="https://www.google.com/maps/dir//160%2F192+Main+St,+Bacchus+Marsh+VIC+3340,+Australia/@-37.6758156,144.3700848,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x6ad695640f453f45:0x937549b868571184!2m2!1d144.4401248!2d-37.6758363">Get Directions</a>
	<div>

	<div>&copy; Mariane Dinis (s3672108) and Ahmet Cem Erdem (s3676200).<script>document.write(new Date().getFullYear());
    </script><br>Images edited with <a href="http://www198.lunapic.com/editor/">LunaPic</a><br>Cinema graphic by <a href="http://www.flaticon.com/authors/freepik">freepik</a> from <a href="http://www.flaticon.com/">Flaticon</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a>. Check out the new logo that I created on <a href="http://logomakr.com" title="Logo Maker">LogoMaker.com</a> https://logomakr.com/3CB7oF3CB7oF</div>
</footer>

</body>

</html>